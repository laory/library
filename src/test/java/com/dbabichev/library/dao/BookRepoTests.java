package com.dbabichev.library.dao;

import com.dbabichev.library.dao.BookRepo;
import com.dbabichev.library.model.Author;
import com.dbabichev.library.model.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("classpath:application-test.properties")
public class BookRepoTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BookRepo bookRepo;

    @Test
    public void createBookTest() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final Book bookToBeSaved = new Book("Test book", author);

        final Book savedBook = bookRepo.save(bookToBeSaved);
        assertThat(savedBook).as("Repo doesn't return saved entity").isNotNull();
        assertThat(savedBook.getId()).as("Repo doesn't return generated id").isNotNull();
        assertThat(savedBook.getTitle()).isEqualTo(bookToBeSaved.getTitle());
        assertThat(savedBook.getAuthor()).isEqualTo(bookToBeSaved.getAuthor());

        final Book foundBook = entityManager.find(Book.class, savedBook.getId());
        assertThat(foundBook).as("Book was not really saved").isEqualTo(savedBook);
    }

    @Test
    public void createBookTestSideEffect() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final Book savedBook2 = entityManager.persistAndFlush(new Book("Test book", author));

        bookRepo.save(new Book("Test book", author));

        final Book foundBook2 = entityManager.find(Book.class, savedBook2.getId());
        assertThat(foundBook2).as("extra Book was changed").isEqualTo(savedBook2);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createBookTestTitleValidation() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        bookRepo.save(new Book(null, author));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createBookTestAuthorValidation() {
        bookRepo.save(new Book("Test", null));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createBookTestAuthorWithWrongIdValidation() {
        bookRepo.save(new Book("Test", new Author(100500L, "Test name")));
    }

    @Test
    public void createBookTestIdValidation() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final long id = 100500L;
        bookRepo.save(new Book(id, "test", author));
        final Book foundBook = entityManager.find(Book.class, id);
        assertThat(foundBook).isNull();
    }

    @Test
    public void findBookByIdTest() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final Book savedBook = entityManager.persistAndFlush(new Book("Test book", author));

        final Book foundBook = bookRepo.findOne(savedBook.getId());
        assertThat(foundBook).as("Repo doesn't return existing entity").isNotNull();
        assertThat(foundBook).as("Book was not really saved").isEqualTo(savedBook);
    }

    @Test
    public void deleteBookByIdTest() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final Book savedBook = entityManager.persistAndFlush(new Book("Test book", author));

        bookRepo.delete(savedBook.getId());

        final Book foundBook = entityManager.find(Book.class, savedBook.getId());

        assertThat(foundBook).as("Repo shouldn't return deleted entity").isNull();
    }

    @Test
    public void deleteBookByIdTestSideEffect() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final Book book = entityManager.persistAndFlush(new Book("Test book", author));
        final Book book2 = entityManager.persistAndFlush(new Book("Test book2", author));

        bookRepo.delete(book.getId());

        final Author foundAuthor = entityManager.find(Author.class, author.getId());
        final Book foundBook2 = entityManager.find(Book.class, book2.getId());
        assertThat(foundAuthor).as("Repo deleted extra entity").isEqualTo(author);
        assertThat(foundBook2).as("Repo deleted extra entity").isEqualTo(book2);
    }

    @Test
    public void updateBookTest() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final Author author2 = entityManager.persistAndFlush(new Author("Test author"));
        final Book book = entityManager.persistAndFlush(new Book("Test book", author));
        final String newTitle = "New test book name";

        book.setTitle(newTitle);
        book.setAuthor(author2);

        bookRepo.save(book);

        final Book foundBook = entityManager.find(Book.class, book.getId());
        assertThat(foundBook).as("Repo doesn't return saved entity").isNotNull();
        assertThat(foundBook.getId()).as("Repo doesn't return generated id").isNotNull();
        assertThat(foundBook.getTitle()).as("Repo didn't change entity").isEqualTo(newTitle);
        assertThat(foundBook.getAuthor()).as("Repo didn't change entity").isEqualTo(author2);
    }

    @Test
    public void updateBookTestSideEffect() {
        final Author author = entityManager.persistAndFlush(new Author("Test author"));
        final Book book = entityManager.persistAndFlush(new Book("Test book", author));
        final Book book2 = entityManager.persistAndFlush(new Book("Test book2", author));

        book.setTitle("New test book name");

        bookRepo.save(book);

        final Book foundBook2 = entityManager.find(Book.class, book2.getId());
        assertThat(foundBook2).as("Repo change extra entity").isEqualTo(book2);
    }
}
