package com.dbabichev.library.dao;

import com.dbabichev.library.model.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("classpath:application-test.properties")
public class AuthorRepoTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AuthorRepo authorRepo;

    @Test
    public void createAuthorTest() {
        final Author authorToBeSaved = new Author("Test user");

        final Author savedAuthor = authorRepo.save(authorToBeSaved);
        assertThat(savedAuthor).as("Repo doesn't return saved entity").isNotNull();
        assertThat(savedAuthor.getId()).as("Repo doesn't return generated id").isNotNull();
        assertThat(savedAuthor.getName()).isEqualTo(authorToBeSaved.getName());

        final Author foundAuthor = entityManager.find(Author.class, savedAuthor.getId());
        assertThat(foundAuthor).as("Author was not really saved").isEqualTo(savedAuthor);
    }

    @Test
    public void createAuthorTestSideEffect() {
        final Author savedAuthor2 = entityManager.persistAndFlush(new Author("Test user"));

        authorRepo.save(new Author("Test user"));

        final Author foundAuthor2 = entityManager.find(Author.class, savedAuthor2.getId());
        assertThat(foundAuthor2).as("extra Author was changed").isEqualTo(savedAuthor2);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createAuthorTestNameValidation() {
        authorRepo.save(new Author(null));
    }

    @Test
    public void createAuthorTestIdValidation() {
        final long id = 100500L;
        authorRepo.save(new Author(id, "test"));
        final Author foundAuthor = entityManager.find(Author.class, id);
        assertThat(foundAuthor).isNull();
    }

    @Test
    public void findAuthorByIdTest() {
        final Author savedAuthor = entityManager.persistAndFlush(new Author("Test user"));

        final Author foundAuthor = authorRepo.findOne(savedAuthor.getId());
        assertThat(foundAuthor).as("Repo doesn't return existing entity").isNotNull();
        assertThat(foundAuthor).as("Author was not really saved").isEqualTo(savedAuthor);
    }

    @Test
    public void deleteAuthorByIdTest() {

        final Author savedAuthor = entityManager.persistAndFlush(new Author("Test user"));

        authorRepo.delete(savedAuthor.getId());

        final Author foundAuthor = entityManager.find(Author.class, savedAuthor.getId());

        assertThat(foundAuthor).as("Repo shouldn't return deleted entity").isNull();
    }

    @Test
    public void deleteAuthorByIdTestSideEffect() {
        final Author author = entityManager.persistAndFlush(new Author("Test user"));
        final Author author2 = entityManager.persistAndFlush(new Author("Test user2"));

        authorRepo.delete(author.getId());

        final Author foundAuthor2 = entityManager.find(Author.class, author2.getId());
        assertThat(foundAuthor2).as("Repo deleted extra entity").isEqualTo(author2);
    }

    @Test
    public void updateAuthorTest() {
        final Author author = entityManager.persistAndFlush(new Author("Test user"));
        final String newName = "New test user name";

        author.setName(newName);

        authorRepo.save(author);

        final Author foundAuthor = entityManager.find(Author.class, author.getId());
        assertThat(foundAuthor).as("Repo doesn't return saved entity").isNotNull();
        assertThat(foundAuthor.getId()).as("Repo doesn't return generated id").isNotNull();
        assertThat(foundAuthor.getName()).as("Repo didn't change entity").isEqualTo(newName);
    }

    @Test
    public void updateAuthorTestSideEffect() {
        final Author author = entityManager.persistAndFlush(new Author("Test user"));
        final Author author2 = entityManager.persistAndFlush(new Author("Test user2"));

        author.setName("New test user name");

        authorRepo.save(author);

        final Author foundAuthor2 = entityManager.find(Author.class, author2.getId());
        assertThat(foundAuthor2).as("Repo change extra entity").isEqualTo(author2);
    }
}
