package com.dbabichev.library.service;

import com.dbabichev.library.dao.AuthorRepo;
import com.dbabichev.library.dao.BookRepo;
import com.dbabichev.library.model.Author;
import com.dbabichev.library.model.Book;
import com.dbabichev.library.service.exception.NonExistingIdException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Copyright 2017 dbabichev
 * User: Dmytro_Babichev
 * Date: 9/1/2017
 * Time: 1:07 PM
 */
@RunWith(SpringRunner.class)
public class LibraryServiceTest {

    private static final long AUTHOR_1_ID = 1L;
    private static final long AUTHOR_2_ID = 2L;
    private static final long AUTHOR_3_ID = 3L;
    private static final String AUTHOR_1_NAME = "Author 1 name";
    private static final String AUTHOR_2_NEW_NAME = "Author 2 name";
    private static final String AUTHOR_3_NAME = "Author 3 name";
    private static final Author AUTHOR_1_NOT_SAVED = new Author(AUTHOR_1_NAME);
    private static final Author AUTHOR_1 = new Author(AUTHOR_1_ID, AUTHOR_1_NAME);
    private static final Author AUTHOR_1_UPDATED = new Author(AUTHOR_1_ID, AUTHOR_2_NEW_NAME);
    private static final Author AUTHOR_3 = new Author(AUTHOR_3_ID, AUTHOR_3_NAME);
    private static final Long BOOK_1_ID = 1L;
    private static final Long BOOK_2_ID = 2L;
    private static final String BOOK_1_TITLE = "Book 1 title";
    private static final String BOOK_2_TITLE = "Book 2 title";
    private static final Book BOOK_1 = new Book(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_1);
    private static final Book BOOK_1_NOT_SAVED = new Book(BOOK_1_TITLE, AUTHOR_1);
    private static final Book BOOK_1_TITLE_UPDATED = new Book(BOOK_1_ID, BOOK_2_TITLE, AUTHOR_1);
    private static final Book BOOK_1_AUTHOR_UPDATED = new Book(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_3);

    @MockBean
    private AuthorRepo authorRepo;
    @MockBean
    private BookRepo bookRepo;

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        @Autowired
        public LibraryService libraryService(final AuthorRepo authorRepo, final BookRepo bookRepo) {
            return new LibraryService(authorRepo, bookRepo);
        }
    }

    @Autowired
    LibraryService libraryService;

    @Before
    public void setUp() {
        initAuthorRepoMock();
        initBookRepoMock();
    }

    private void initAuthorRepoMock() {
        Mockito.when(authorRepo.findOne(AUTHOR_1_ID)).thenReturn(AUTHOR_1);
        Mockito.when(authorRepo.findOne(AUTHOR_2_ID)).thenReturn(null);
        Mockito.when(authorRepo.findOne(AUTHOR_3_ID)).thenReturn(AUTHOR_3);
        Mockito.when(authorRepo.exists(AUTHOR_1_ID)).thenReturn(true);
        Mockito.when(authorRepo.exists(AUTHOR_2_ID)).thenReturn(false);
        Mockito.when(authorRepo.exists(AUTHOR_3_ID)).thenReturn(true);
        Mockito.when(authorRepo.save(AUTHOR_1_NOT_SAVED)).thenReturn(AUTHOR_1);
        Mockito.when(authorRepo.save(AUTHOR_1_UPDATED)).thenReturn(AUTHOR_1_UPDATED);
        doNothing().when(authorRepo).delete(AUTHOR_1);
        Mockito.when(authorRepo.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Arrays.asList(AUTHOR_1, AUTHOR_3)));
    }

    private void initBookRepoMock() {
        Mockito.when(bookRepo.findOne(BOOK_1_ID)).thenReturn(BOOK_1);
        Mockito.when(bookRepo.findOne(BOOK_2_ID)).thenReturn(null);
        Mockito.when(bookRepo.exists(BOOK_1_ID)).thenReturn(true);
        Mockito.when(bookRepo.exists(BOOK_2_ID)).thenReturn(false);
        Mockito.when(bookRepo.save(BOOK_1_NOT_SAVED)).thenReturn(BOOK_1);
        Mockito.when(bookRepo.save(BOOK_1_TITLE_UPDATED)).thenReturn(BOOK_1_TITLE_UPDATED);
        Mockito.when(bookRepo.save(BOOK_1_AUTHOR_UPDATED)).thenReturn(BOOK_1_AUTHOR_UPDATED);
        doNothing().when(bookRepo).delete(BOOK_1);
        Mockito.when(bookRepo.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.singletonList(BOOK_1)));
    }

    @Test
    public void getAuthors() throws Exception {
        final PageRequest pageRequest = new PageRequest(0, 100);
        libraryService.getAuthors(pageRequest);

        verify(authorRepo, times(1)).findAll(pageRequest);
        verifyNoMoreInteractions(authorRepo);
        verifyZeroInteractions(bookRepo);
    }

    @Test
    public void getBooks() throws Exception {
        final PageRequest pageRequest = new PageRequest(0, 100);
        libraryService.getBooks(pageRequest);

        verify(bookRepo, times(1)).findAll(pageRequest);
        verifyNoMoreInteractions(bookRepo);
        verifyZeroInteractions(authorRepo);
    }

    @Test
    public void addAuthor() throws Exception {
        libraryService.addAuthor(AUTHOR_1_NAME);

        verify(authorRepo, times(1)).save(AUTHOR_1_NOT_SAVED);
        verifyNoMoreInteractions(authorRepo);
        verifyZeroInteractions(bookRepo);
    }

    @Test
    public void addBookWithExistingAuthor() throws Exception {
        libraryService.addBook(BOOK_1_TITLE, AUTHOR_1_ID);

        verify(authorRepo, times(1)).findOne(AUTHOR_1_ID);
        verify(bookRepo, times(1)).save(BOOK_1_NOT_SAVED);
        verifyNoMoreInteractions(authorRepo);
        verifyNoMoreInteractions(bookRepo);
    }

    @Test(expected = NonExistingIdException.class)
    public void addBookWithNonExistingAuthor() throws Exception {
        try {
            libraryService.addBook(BOOK_1_TITLE, AUTHOR_2_ID);
        } finally {
            verify(authorRepo, times(1)).findOne(AUTHOR_2_ID);
            verify(authorRepo, never()).save(any(Author.class));
            verifyNoMoreInteractions(authorRepo);
            verifyZeroInteractions(bookRepo);
        }
    }

    @Test
    public void deleteExistingAuthor() throws Exception {
        libraryService.deleteAuthor(AUTHOR_1_ID);

        verify(authorRepo, times(1)).exists(AUTHOR_1_ID);
        verify(authorRepo, times(1)).delete(AUTHOR_1_ID);
        verifyNoMoreInteractions(authorRepo);
        verifyZeroInteractions(bookRepo);
    }

    @Test(expected = NonExistingIdException.class)
    public void deleteNonExistingAuthor() throws Exception {
        try {
            libraryService.deleteAuthor(AUTHOR_2_ID);
        } finally {
            verify(authorRepo, times(1)).exists(AUTHOR_2_ID);
            verifyNoMoreInteractions(authorRepo);
            verifyZeroInteractions(bookRepo);
        }
    }

    @Test
    public void deleteExistingBook() throws Exception {
        libraryService.deleteBook(BOOK_1_ID);

        verify(bookRepo, times(1)).exists(BOOK_1_ID);
        verify(bookRepo, times(1)).delete(BOOK_1_ID);
        verifyNoMoreInteractions(bookRepo);
        verifyZeroInteractions(authorRepo);
    }

    @Test(expected = NonExistingIdException.class)
    public void deleteNonExistingBook() throws Exception {
        try {
            libraryService.deleteBook(BOOK_2_ID);
        } finally {
            verify(bookRepo, times(1)).exists(BOOK_2_ID);
            verifyNoMoreInteractions(bookRepo);
            verifyZeroInteractions(authorRepo);
        }
    }

    @Test
    public void updateExistingAuthor() throws Exception {
        libraryService.updateAuthor(AUTHOR_1_ID, AUTHOR_2_NEW_NAME);

        verify(authorRepo, times(1)).findOne(AUTHOR_1_ID);
        verify(authorRepo, times(1)).save(AUTHOR_1_UPDATED);
        verifyNoMoreInteractions(authorRepo);
        verifyZeroInteractions(bookRepo);
    }

    @Test(expected = NonExistingIdException.class)
    public void updateNonExistingAuthor() throws Exception {
        try {
            libraryService.updateAuthor(AUTHOR_2_ID, AUTHOR_2_NEW_NAME);
        } finally {
            verify(authorRepo, times(1)).findOne(AUTHOR_2_ID);
            verifyNoMoreInteractions(authorRepo);
            verifyZeroInteractions(bookRepo);
        }
    }

    @Test
    public void updateTitleOfExistingBook() throws Exception {
        libraryService.updateBook(BOOK_1_ID, BOOK_2_TITLE, AUTHOR_1_ID);

        verify(authorRepo, times(1)).findOne(AUTHOR_1_ID);
        verify(bookRepo, times(1)).findOne(BOOK_1_ID);
        verify(bookRepo, times(1)).save(BOOK_1_TITLE_UPDATED);
        verifyNoMoreInteractions(authorRepo);
        verifyNoMoreInteractions(bookRepo);
    }

    @Test
    public void updateAuthorOfExistingBook() throws Exception {
        libraryService.updateBook(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_3_ID);

        verify(authorRepo, times(1)).findOne(AUTHOR_3_ID);
        verify(bookRepo, times(1)).findOne(BOOK_1_ID);
        verify(bookRepo, times(1)).save(BOOK_1_AUTHOR_UPDATED);
        verifyNoMoreInteractions(authorRepo);
        verifyNoMoreInteractions(bookRepo);
    }

    @Test(expected = NonExistingIdException.class)
    public void updateNonExistingAuthorOfExistingBook() throws Exception {
        try {
            libraryService.updateBook(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_2_ID);
        } finally {
            verify(bookRepo, times(1)).findOne(BOOK_1_ID);
            verify(authorRepo, times(1)).findOne(AUTHOR_2_ID);
            verifyNoMoreInteractions(authorRepo);
            verifyNoMoreInteractions(bookRepo);
        }
    }

    @Test(expected = NonExistingIdException.class)
    public void updateAuthorOfNonExistingBook() throws Exception {
        try {
            libraryService.updateBook(BOOK_2_ID, BOOK_1_TITLE, AUTHOR_3_ID);
        } finally {
            verify(bookRepo, times(1)).findOne(BOOK_2_ID);
            verifyNoMoreInteractions(bookRepo);
            verifyZeroInteractions(authorRepo);
        }
    }

    @Test
    public void getExistingBook() throws Exception {
        libraryService.getBook(BOOK_1_ID);

        verify(bookRepo, times(1)).findOne(BOOK_1_ID);
        verifyNoMoreInteractions(bookRepo);
        verifyZeroInteractions(authorRepo);
    }

    @Test(expected = NonExistingIdException.class)
    public void getNonExistingBook() throws Exception {
        try {
            libraryService.getBook(BOOK_2_ID);
        } finally {
            verify(bookRepo, times(1)).findOne(BOOK_2_ID);
            verifyNoMoreInteractions(bookRepo);
            verifyZeroInteractions(authorRepo);
        }
    }

    @Test
    public void getExistingAuthor() throws Exception {
        libraryService.getAuthor(AUTHOR_1_ID);

        verify(authorRepo).findOne(AUTHOR_1_ID);
        verifyNoMoreInteractions(authorRepo);
        verifyZeroInteractions(bookRepo);
    }

    @Test(expected = NonExistingIdException.class)
    public void getNonExistingAuthor() throws Exception {
        try {
            libraryService.getAuthor(AUTHOR_2_ID);
        } finally {
            verify(authorRepo).findOne(AUTHOR_2_ID);
            verifyNoMoreInteractions(bookRepo);
            verifyZeroInteractions(authorRepo);
        }
    }

}