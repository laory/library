package com.dbabichev.library.controller;

import com.dbabichev.library.controller.dto.AuthorDto;
import com.dbabichev.library.controller.dto.BookDto;
import com.dbabichev.library.controller.dto.ModelMapper;
import com.dbabichev.library.model.Author;
import com.dbabichev.library.model.Book;
import com.dbabichev.library.service.LibraryService;
import com.dbabichev.library.service.exception.NonExistingIdException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Copyright 2017 dbabichev
 * User: dbabichev
 * Date: 03/9/17
 * Time: 12:56 AM
 */
@RunWith(SpringRunner.class)
@WebMvcTest(LibraryRestController.class)
public class LibraryRestControllerTest {

    private static final long AUTHOR_1_ID = 1L;
    private static final long AUTHOR_3_ID = 3L;
    private static final String AUTHOR_1_NAME = "Author 1 name";
    private static final String AUTHOR_3_NAME = "Author 3 name";
    private static final Author AUTHOR_1 = new Author(AUTHOR_1_ID, AUTHOR_1_NAME);
    private static final Author AUTHOR_3 = new Author(AUTHOR_3_ID, AUTHOR_3_NAME);
    private static final Long BOOK_1_ID = 1L;
    private static final String BOOK_1_TITLE = "Book 1 title";
    private static final Book BOOK_1 = new Book(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_1);

    @Autowired
    private MockMvc mvc;
    @MockBean
    private LibraryService libraryService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    ModelMapper modelMapper;

    @TestConfiguration
    static class LibraryRestControllerTestConfiguration {
        @Bean
        public ModelMapper modelMapper() {
            return new ModelMapper();
        }
    }

    @Test
    public void booksPage() throws Exception {
        final PageImpl<Book> bookPage = new PageImpl<>(Arrays.asList(BOOK_1), new PageRequest(0, 10), 1);

        when(libraryService.getBooks(any(PageRequest.class))).thenReturn(bookPage);

        mvc.perform(get("/books").accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(bookPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getBooks(new PageRequest(0, 10, new Sort(Sort.Direction.ASC, "title")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void authorsPage() throws Exception {
        final PageImpl<Author> authorPage = new PageImpl<>(Arrays.asList(AUTHOR_1, AUTHOR_3), new PageRequest(0, 10), 2);

        when(libraryService.getAuthors(any(PageRequest.class))).thenReturn(authorPage);

        mvc.perform(get("/authors").accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(authorPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getAuthors(new PageRequest(0, 10, new Sort(Sort.Direction.ASC, "name")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void booksPageWithPage() throws Exception {
        final int page = 5;
        final PageImpl<Book> bookPage = new PageImpl<>(Arrays.asList(BOOK_1), new PageRequest(page, 10), 1);

        when(libraryService.getBooks(any(PageRequest.class))).thenReturn(bookPage);

        mvc.perform(get("/books").param("page", "" + page).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(bookPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getBooks(new PageRequest(page, 10, new Sort(Sort.Direction.ASC, "title")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void booksPageWithLimit() throws Exception {
        final int page = 0;
        final int limit = 1000;
        final PageImpl<Book> bookPage = new PageImpl<>(Arrays.asList(BOOK_1), new PageRequest(page, limit), 1);

        when(libraryService.getBooks(any(PageRequest.class))).thenReturn(bookPage);

        mvc.perform(get("/books").param("limit", "" + limit).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(bookPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getBooks(new PageRequest(page, limit, new Sort(Sort.Direction.ASC, "title")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void booksPageWithParams() throws Exception {
        final int page = 12;
        final int limit = 1000;
        final PageImpl<Book> bookPage = new PageImpl<>(Arrays.asList(BOOK_1), new PageRequest(page, limit), 1);

        when(libraryService.getBooks(any(PageRequest.class))).thenReturn(bookPage);

        mvc.perform(get("/books").param("page", "" + page).param("limit", "" + limit).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(bookPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getBooks(new PageRequest(page, limit, new Sort(Sort.Direction.ASC, "title")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void authorsPageWithPage() throws Exception {
        final int page = 5;
        final PageImpl<Author> authorPage = new PageImpl<>(Arrays.asList(AUTHOR_1, AUTHOR_3), new PageRequest(page, 10), 2);

        when(libraryService.getAuthors(any(PageRequest.class))).thenReturn(authorPage);

        mvc.perform(get("/authors").param("page", "" + page).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(authorPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getAuthors(new PageRequest(page, 10, new Sort(Sort.Direction.ASC, "name")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void authorsPageWithLimit() throws Exception {
        final int page = 0;
        final int limit = 1000;
        final PageImpl<Author> authorPage = new PageImpl<>(Arrays.asList(AUTHOR_1, AUTHOR_3), new PageRequest(page, limit), 2);

        when(libraryService.getAuthors(any(PageRequest.class))).thenReturn(authorPage);

        mvc.perform(get("/authors").param("limit", "" + limit).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(authorPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getAuthors(new PageRequest(page, limit, new Sort(Sort.Direction.ASC, "name")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void authorsPageWithParams() throws Exception {
        final int page = 12;
        final int limit = 1000;
        final PageImpl<Author> authorPage = new PageImpl<>(Arrays.asList(AUTHOR_1, AUTHOR_3), new PageRequest(page, limit), 2);

        when(libraryService.getAuthors(any(PageRequest.class))).thenReturn(authorPage);

        mvc.perform(get("/authors").param("page", "" + page).param("limit", "" + limit).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(authorPage.map(modelMapper::convert))));

        verify(libraryService, times(1)).getAuthors(new PageRequest(page, limit, new Sort(Sort.Direction.ASC, "name")));
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void createBook() throws Exception {
        final BookDto newBook = new BookDto(BOOK_1_TITLE, AUTHOR_1_ID);

        when(libraryService.addBook(BOOK_1_TITLE, AUTHOR_1_ID)).thenReturn(BOOK_1);

        mvc.perform(
                post("/books")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(newBook)))
                .andExpect(status().isCreated())
                .andExpect(content().json(objectMapper.writeValueAsString(modelMapper.convert(BOOK_1))))
                .andExpect(header().string("Location", "/books/" + BOOK_1_ID));

        verify(libraryService, times(1)).addBook(BOOK_1_TITLE, AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void createBookNonExistingAuthor() throws Exception {
        final BookDto newBook = new BookDto(BOOK_1_TITLE, AUTHOR_1_ID);

        when(libraryService.addBook(BOOK_1_TITLE, AUTHOR_1_ID)).thenThrow(new NonExistingIdException(""));

        mvc.perform(
                post("/books")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(newBook)))
                .andExpect(status().isNotFound());

        verify(libraryService, times(1)).addBook(BOOK_1_TITLE, AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void createBookNotValidRequest() throws Exception {
        mvc.perform(
                post("/books")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto())))
                .andExpect(status().isBadRequest());
        mvc.perform(
                post("/books")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto(BOOK_1_TITLE, null))))
                .andExpect(status().isBadRequest());
        mvc.perform(
                post("/books")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto(null, AUTHOR_1_ID))))
                .andExpect(status().isBadRequest());
        mvc.perform(
                post("/books")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto("", AUTHOR_1_ID))))
                .andExpect(status().isBadRequest());

        verifyZeroInteractions(libraryService);
    }

    @Test
    public void createAuthor() throws Exception {
        final AuthorDto newAuthor = new AuthorDto(AUTHOR_1_NAME);

        when(libraryService.addAuthor(AUTHOR_1_NAME)).thenReturn(AUTHOR_1);

        mvc.perform(
                post("/authors")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(newAuthor)))
                .andExpect(status().isCreated())
                .andExpect(content().json(objectMapper.writeValueAsString(modelMapper.convert(AUTHOR_1))))
                .andExpect(header().string("Location", "/authors/" + AUTHOR_1_ID));

        verify(libraryService, times(1)).addAuthor(AUTHOR_1_NAME);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void createAuthorNotValidRequest() throws Exception {
        mvc.perform(
                post("/authors")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new AuthorDto())))
                .andExpect(status().isBadRequest());
        mvc.perform(
                post("/authors")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new AuthorDto(""))))
                .andExpect(status().isBadRequest());

        verifyZeroInteractions(libraryService);
    }

    @Test
    public void updateBook() throws Exception {
        final BookDto updatedBook = new BookDto(BOOK_1_TITLE, AUTHOR_1_ID);

        when(libraryService.updateBook(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_1_ID)).thenReturn(BOOK_1);

        mvc.perform(
                put("/books/" + BOOK_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(updatedBook)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(modelMapper.convert(BOOK_1))))
                .andExpect(header().string("Location", nullValue()));

        verify(libraryService, times(1)).updateBook(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void updateNonExistingBook() throws Exception {
        final BookDto updatedBook = new BookDto(BOOK_1_TITLE, AUTHOR_1_ID);

        when(libraryService.updateBook(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_1_ID)).thenThrow(new NonExistingIdException(""));

        mvc.perform(
                put("/books/" + BOOK_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(updatedBook)))
                .andExpect(status().isNotFound());

        verify(libraryService, times(1)).updateBook(BOOK_1_ID, BOOK_1_TITLE, AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void updateAuthor() throws Exception {
        final AuthorDto updatedAuthor = new AuthorDto(AUTHOR_1_NAME);

        when(libraryService.updateAuthor(AUTHOR_1_ID, AUTHOR_1_NAME)).thenReturn(AUTHOR_1);

        mvc.perform(
                put("/authors/" + AUTHOR_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(updatedAuthor)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(modelMapper.convert(AUTHOR_1))))
                .andExpect(header().string("Location", nullValue()));

        verify(libraryService, times(1)).updateAuthor(AUTHOR_1_ID, AUTHOR_1_NAME);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void updateNonExistingAuthor() throws Exception {
        final AuthorDto updatedAuthor = new AuthorDto(AUTHOR_1_NAME);

        when(libraryService.updateAuthor(AUTHOR_1_ID, AUTHOR_1_NAME)).thenThrow(new NonExistingIdException(""));

        mvc.perform(
                put("/authors/" + AUTHOR_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(updatedAuthor)))
                .andExpect(status().isNotFound());

        verify(libraryService, times(1)).updateAuthor(AUTHOR_1_ID, AUTHOR_1_NAME);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void updateBookNotValidRequest() throws Exception {
        mvc.perform(
                put("/books/" + BOOK_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto())))
                .andExpect(status().isBadRequest());
        mvc.perform(
                put("/books/" + BOOK_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto(BOOK_1_TITLE, null))))
                .andExpect(status().isBadRequest());
        mvc.perform(
                put("/books/" + BOOK_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto(null, AUTHOR_1_ID))))
                .andExpect(status().isBadRequest());
        mvc.perform(
                put("/books/" + BOOK_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new BookDto("", AUTHOR_1_ID))))
                .andExpect(status().isBadRequest());

        verifyZeroInteractions(libraryService);
    }

    @Test
    public void updateAuthorNotValidRequest() throws Exception {
        mvc.perform(
                put("/authors/" + AUTHOR_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new AuthorDto())))
                .andExpect(status().isBadRequest());
        mvc.perform(
                put("/authors/" + AUTHOR_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(new AuthorDto(""))))
                .andExpect(status().isBadRequest());

        verifyZeroInteractions(libraryService);
    }

    @Test
    public void deleteBook() throws Exception {
        doNothing().when(libraryService).deleteBook(BOOK_1_ID);

        mvc.perform(
                delete("/books/" + BOOK_1_ID))
                .andExpect(status().isOk());

        verify(libraryService, times(1)).deleteBook(BOOK_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void deleteNonExistingBook() throws Exception {
        doThrow(new NonExistingIdException("")).when(libraryService).deleteBook(BOOK_1_ID);

        mvc.perform(
                delete("/books/" + BOOK_1_ID))
                .andExpect(status().isNotFound());

        verify(libraryService, times(1)).deleteBook(BOOK_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void deleteAuthor() throws Exception {
        doNothing().when(libraryService).deleteAuthor(AUTHOR_1_ID);

        mvc.perform(
                delete("/authors/" + AUTHOR_1_ID))
                .andExpect(status().isOk());

        verify(libraryService, times(1)).deleteAuthor(AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void deleteNonExistingAuthor() throws Exception {
        doThrow(new NonExistingIdException("")).when(libraryService).deleteAuthor(AUTHOR_1_ID);

        mvc.perform(
                delete("/authors/" + AUTHOR_1_ID))
                .andExpect(status().isNotFound());

        verify(libraryService, times(1)).deleteAuthor(AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void getBook() throws Exception {
        when(libraryService.getBook(BOOK_1_ID)).thenReturn(BOOK_1);

        mvc.perform(
                get("/books/" + BOOK_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(modelMapper.convert(BOOK_1))));

        verify(libraryService, times(1)).getBook(BOOK_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void getNonExistingBook() throws Exception {
        when(libraryService.getBook(BOOK_1_ID)).thenThrow(new NonExistingIdException(""));

        mvc.perform(
                get("/books/" + BOOK_1_ID))
                .andExpect(status().isNotFound());

        verify(libraryService, times(1)).getBook(BOOK_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void getAuthor() throws Exception {
        when(libraryService.getAuthor(AUTHOR_1_ID)).thenReturn(AUTHOR_1);

        mvc.perform(
                get("/authors/" + AUTHOR_1_ID)
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(modelMapper.convert(AUTHOR_1))));

        verify(libraryService, times(1)).getAuthor(AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void getNonExistingAuthor() throws Exception {
        when(libraryService.getAuthor(AUTHOR_1_ID)).thenThrow(new NonExistingIdException(""));

        mvc.perform(
                get("/authors/" + AUTHOR_1_ID))
                .andExpect(status().isNotFound());

        verify(libraryService, times(1)).getAuthor(AUTHOR_1_ID);
        verifyNoMoreInteractions(libraryService);
    }

    @Test
    public void invalidUri() throws Exception {
        mvc.perform(
                get("/authors/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                put("/authors/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                delete("/authors/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                post("/authors/dsklf"))
                .andExpect(status().isNotFound());

        mvc.perform(
                get("/books/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                put("/books/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                delete("/books/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                post("/books/dsklf"))
                .andExpect(status().isNotFound());

        mvc.perform(
                get("/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                put("/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                delete("/dsklf"))
                .andExpect(status().isNotFound());
        mvc.perform(
                post("/dsklf"))
                .andExpect(status().isNotFound());

        verifyZeroInteractions(libraryService);
    }
}