package com.dbabichev.library.dao;

import com.dbabichev.library.model.Book;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Copyright 2017 dbabichev
 * User: dbabichev
 * Date: 26/8/17
 * Time: 10:43 PM
 */
public interface BookRepo extends PagingAndSortingRepository<Book, Long> {
}
