package com.dbabichev.library.service.exception;

/**
 * Copyright 2017 dbabichev
 * User: Dmytro_Babichev
 * Date: 8/31/2017
 * Time: 12:32 PM
 */
public class NonExistingIdException extends RuntimeException {

    public NonExistingIdException(final String message) {
        super(message);
    }
}
