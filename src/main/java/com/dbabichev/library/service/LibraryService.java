package com.dbabichev.library.service;

import com.dbabichev.library.dao.AuthorRepo;
import com.dbabichev.library.dao.BookRepo;
import com.dbabichev.library.model.Author;
import com.dbabichev.library.model.Book;
import com.dbabichev.library.service.exception.NonExistingIdException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Copyright 2017 dbabichev
 * User: dbabichev
 * Date: 26/8/17
 * Time: 11:57 PM
 */
@Service
@Transactional
public class LibraryService {

    private static final Logger LOG = LoggerFactory.getLogger(LibraryService.class);

    private final AuthorRepo authorRepo;
    private final BookRepo bookRepo;

    @Autowired
    public LibraryService(final AuthorRepo authorRepo, final BookRepo bookRepo) {
        this.authorRepo = authorRepo;
        this.bookRepo = bookRepo;
    }

    public Page<Author> getAuthors(final PageRequest pageRequest) {
        return authorRepo.findAll(pageRequest);
    }

    public Page<Book> getBooks(final PageRequest pageRequest) {
        return bookRepo.findAll(pageRequest);
    }

    public Author addAuthor(final String authorName) {
        final Author author = new Author(authorName);
        return authorRepo.save(author);
    }

    public Book addBook(final String title, final Long authorId) {
        final Author author = getAuthor(authorId);
        final Book book = new Book(title, author);
        return bookRepo.save(book);
    }

    public void deleteAuthor(final Long authorId) {
        if (!authorRepo.exists(authorId)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Author {} doesn't exist", authorId);
            }
            throw new NonExistingIdException("Author id: " + authorId + " does not exist");
        }
        authorRepo.delete(authorId);
    }

    public void deleteBook(final Long bookId) {
        if (!bookRepo.exists(bookId)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Book {} doesn't exist", bookId);
            }
            throw new NonExistingIdException("Book id: " + bookId + " does not exist");
        }
        bookRepo.delete(bookId);
    }

    public Author updateAuthor(final Long id, final String name) {
        final Author author = getAuthor(id);
        author.setName(name);
        return authorRepo.save(author);
    }

    public Book updateBook(final Long id, final String title, final Long authorId) {
        final Book book = getBook(id);
        final Author author = getAuthor(authorId);
        book.setTitle(title);
        book.setAuthor(author);
        return bookRepo.save(book);
    }

    public Book getBook(final Long id) {
        final Book book = bookRepo.findOne(id);
        if (book == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Book {} doesn't exist", id);
            }
            throw new NonExistingIdException("Book id: " + id + " does not exist");
        }
        return book;
    }

    public Author getAuthor(final Long authorId) {
        final Author author = authorRepo.findOne(authorId);
        if (author == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Author {} doesn't exist", authorId);
            }
            throw new NonExistingIdException("Author id: " + authorId + " does not exist");
        }
        return author;
    }
}
