package com.dbabichev.library.model;

import javax.persistence.*;

/**
 * Copyright 2017 dbabichev
 * User: dbabichev
 * Date: 26/8/17
 * Time: 10:37 PM
 */
@Entity
public class Book {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String title;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Author author;

    protected Book() {
    }

    public Book(final Long id, final String title, final Author author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    public Book(final String title, final Author author) {
        this(null, title, author);
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(final Author author) {
        this.author = author;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Book book = (Book) o;

        if (id != null ? !id.equals(book.id) : book.id != null) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        return author != null ? author.equals(book.author) : book.author == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author=" + author +
                '}';
    }
}
