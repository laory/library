package com.dbabichev.library.controller;

import com.dbabichev.library.controller.dto.AuthorDto;
import com.dbabichev.library.controller.dto.BookDto;
import com.dbabichev.library.controller.dto.ModelMapper;
import com.dbabichev.library.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Copyright 2017 dbabichev
 * User: dbabichev
 * Date: 27/8/17
 * Time: 9:00 PM
 */
@Controller
public class LibraryController {

    public static final String DEFAULT_AUTHOR_ORDER_BY = "name";
    public static final Sort.Direction DEFAULT_DIRECTION = Sort.Direction.ASC;
    public static final int AUTHORS_SELECT_LIMIT = 1000;

    private final LibraryService libraryService;
    private final ModelMapper modelMapper;

    @Autowired
    public LibraryController(final LibraryService libraryService, final ModelMapper modelMapper) {
        this.libraryService = libraryService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/")
    public String mainPage() {
        return "redirect:/booksList";
    }

    @GetMapping("/booksList")
    public String booksPage(@RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
                            @RequestParam(value = "limit", required = false, defaultValue = "10") final Integer limit,
                            @RequestParam(value = "orderBy", required = false, defaultValue = "title") final String orderBy,
                            @RequestParam(value = "direction", required = false, defaultValue = "ASC") final Sort.Direction direction,
                            final Map<String, Object> model) {
        final Page<BookDto> booksPageObj = libraryService.getBooks(new PageRequest(page, limit, direction, orderBy))
                .map(modelMapper::convert);
        model.put("pageObj", booksPageObj);
        model.put("page", "booksList");
        return "index";
    }

    @GetMapping("/authorsList")
    public String authorsPage(@RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
                              @RequestParam(value = "limit", required = false, defaultValue = "10") final Integer limit,
                              @RequestParam(value = "orderBy", required = false, defaultValue = "name") final String orderBy,
                              @RequestParam(value = "direction", required = false, defaultValue = "ASC") final Sort.Direction direction,
                              final Map<String, Object> model) {
        final Page<AuthorDto> authorPageObj = libraryService.getAuthors(new PageRequest(page, limit, direction, orderBy))
                .map(modelMapper::convert);
        model.put("pageObj", authorPageObj);
        model.put("page", "authorsList");
        return "index";
    }

    @GetMapping("/createBook")
    public String createBook(final Map<String, Object> model) {
        final Page<AuthorDto> authorPageObj = libraryService.getAuthors(new PageRequest(0, AUTHORS_SELECT_LIMIT, DEFAULT_DIRECTION, DEFAULT_AUTHOR_ORDER_BY))
                .map(modelMapper::convert);
        model.put("page", "createBookPage");
        model.put("authorsPageObj", authorPageObj);
        return "index";
    }

    @GetMapping("/createAuthor")
    public String createAuthor(final Map<String, Object> model) {
        model.put("page", "createAuthorPage");
        return "index";
    }

    @GetMapping("/updateBook/{bookId}")
    public String updateBook(@PathVariable final Long bookId, final Map<String, Object> model) {
        final Page<AuthorDto> authorPageObj = libraryService.getAuthors(new PageRequest(0, AUTHORS_SELECT_LIMIT, DEFAULT_DIRECTION, DEFAULT_AUTHOR_ORDER_BY))
                .map(modelMapper::convert);
        final BookDto book = modelMapper.convert(libraryService.getBook(bookId));
        model.put("page", "updateBookPage");
        model.put("authorsPageObj", authorPageObj);
        model.put("book", book);
        return "index";
    }

    @GetMapping("/updateAuthor/{authorId}")
    public String updateAuthor(@PathVariable final Long authorId, final Map<String, Object> model) {
        final AuthorDto author = modelMapper.convert(libraryService.getAuthor(authorId));
        model.put("page", "updateAuthorPage");
        model.put("author", author);
        return "index";
    }
}
