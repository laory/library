package com.dbabichev.library.controller;

import com.dbabichev.library.controller.dto.AuthorDto;
import com.dbabichev.library.controller.dto.BookDto;
import com.dbabichev.library.controller.dto.ModelMapper;
import com.dbabichev.library.model.Author;
import com.dbabichev.library.model.Book;
import com.dbabichev.library.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * Copyright 2017 dbabichev
 * User: dbabichev
 * Date: 27/8/17
 * Time: 9:00 PM
 */
@RestController
public class LibraryRestController {

    private final LibraryService libraryService;
    private final HttpServletRequest request;
    private final HttpServletResponse response;
    private final ModelMapper modelMapper;

    @Autowired
    public LibraryRestController(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse,
                                 final LibraryService libraryService, final ModelMapper modelMapper) {
        this.request = httpServletRequest;
        this.response = httpServletResponse;
        this.libraryService = libraryService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/authors", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Page<AuthorDto> getAuthors(@RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
                                      @RequestParam(value = "limit", required = false, defaultValue = "10") final Integer limit,
                                      @RequestParam(value = "orderBy", required = false, defaultValue = "name") final String orderBy,
                                      @RequestParam(value = "direction", required = false, defaultValue = "ASC") final Sort.Direction direction) {
        final PageRequest pageRequest = new PageRequest(page, limit, new Sort(direction, orderBy));
        return libraryService.getAuthors(pageRequest).map(modelMapper::convert);
    }

    @GetMapping(value = "/books", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Page<BookDto> getBooks(@RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
                                  @RequestParam(value = "limit", required = false, defaultValue = "10") final Integer limit,
                                  @RequestParam(value = "orderBy", required = false, defaultValue = "title") final String orderBy,
                                  @RequestParam(value = "direction", required = false, defaultValue = "ASC") final Sort.Direction direction) {
        final PageRequest pageRequest = new PageRequest(page, limit, new Sort(direction, orderBy));
        return libraryService.getBooks(pageRequest).map(modelMapper::convert);
    }

    @GetMapping(value = "/authors/{authorId:\\d+}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public AuthorDto getAuthor(@PathVariable("authorId") final Long authorId) {
        return modelMapper.convert(libraryService.getAuthor(authorId));
    }

    @PostMapping(value = "/authors", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AuthorDto createAuthor(@Valid @RequestBody final AuthorDto authorDto) {
        final Author author = libraryService.addAuthor(authorDto.name);
        setLocationHeader("/authors/{id}", author.getId());
        return modelMapper.convert(author);
    }

    @PutMapping(value = "/authors/{authorId:\\d+}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public AuthorDto updateAuthor(@PathVariable("authorId") final Long authorId, @Valid @RequestBody final AuthorDto authorDto) {
        return modelMapper.convert(libraryService.updateAuthor(authorId, authorDto.name));
    }

    @DeleteMapping(value = "/authors/{authorId:\\d+}")
    public void deleteAuthor(@PathVariable("authorId") final Long authorId) {
        libraryService.deleteAuthor(authorId);
    }

    @GetMapping(value = "/books/{bookId:\\d+}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookDto getBook(@PathVariable("bookId") final Long bookId) {
        return modelMapper.convert(libraryService.getBook(bookId));
    }

    @PostMapping(value = "/books", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public BookDto createBook(@Valid @RequestBody final BookDto bookDto) {
        final Book book = libraryService.addBook(bookDto.title, bookDto.authorId);
        setLocationHeader("/books/{id}", book.getId());
        return modelMapper.convert(book);
    }

    @PutMapping(value = "/books/{bookId:\\d+}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookDto updateBook(@PathVariable("bookId") final Long bookId, @Valid @RequestBody final BookDto bookDto) {
        return modelMapper.convert(libraryService.updateBook(bookId, bookDto.title, bookDto.authorId));
    }

    @DeleteMapping(value = "/books/{bookId:\\d+}")
    public void deleteBook(@PathVariable("bookId") final Long bookId) {
        libraryService.deleteBook(bookId);
    }

    private void setLocationHeader(final String path, final Long id) {
        final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromPath(path);
        final String uri = uriComponentsBuilder.buildAndExpand(id).toUriString();
        response.addHeader("Location", uri);
    }
}
