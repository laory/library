package com.dbabichev.library.controller;

import com.dbabichev.library.service.exception.NonExistingIdException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalResponseEntityExceptionHandler.class);

    @ExceptionHandler(NonExistingIdException.class)
    protected ResponseEntity<Object> handleNonExistingIdException(final NonExistingIdException e, final WebRequest request) {
        return handleExceptionInternal(e, e.getMessage(),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolationException(final DataIntegrityViolationException e, final WebRequest request) {
        return handleExceptionInternal(e, "Violation of DB integrity",
                new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException e,
                                                                  final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final String errorMessage = e.getBindingResult().getAllErrors().stream()
                .map(ObjectError::toString)
                .collect(Collectors.joining(";"));
        return handleExceptionInternal(e, errorMessage,
                headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(final Exception ex, final Object body,
                                                             final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        LOG.error("Unable to handle web request {} due to internal error", request, ex);
        return super.handleExceptionInternal(ex, body, headers, status, request);
    }
}
