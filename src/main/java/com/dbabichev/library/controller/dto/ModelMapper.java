package com.dbabichev.library.controller.dto;

import com.dbabichev.library.model.Author;
import com.dbabichev.library.model.Book;
import org.springframework.stereotype.Component;

/**
 * Copyright 2017 Getty Images
 * User: Dmytro_Babichev
 * Date: 9/5/2017
 * Time: 1:18 PM
 */
@Component
public class ModelMapper {

    public AuthorDto convert(final Author author) {
        return new AuthorDto(author.getId(), author.getName());
    }

    public BookDto convert(final Book book) {
        return new BookDto(book.getId(), book.getTitle(), book.getAuthor().getId(), book.getAuthor().getName());
    }

    public Author convert(final AuthorDto author) {
        return new Author(author.id, author.name);
    }

    public Book convert(final BookDto book) {
        return new Book(book.id, book.title, new Author(book.authorId, book.authorName));
    }
}
