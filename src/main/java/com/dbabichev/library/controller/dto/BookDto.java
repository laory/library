package com.dbabichev.library.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Copyright 2017 dbabichev
 * User: Dmytro_Babichev
 * Date: 8/30/2017
 * Time: 2:22 PM
 */
public class BookDto {
    public Long id;
    @NotBlank(message = "Provide non-blank book's title")
    public String title;
    @NotNull(message = "Author id is required")
    public Long authorId;
    public String authorName;

    public BookDto(final String title, final Long authorId) {
        this(null, title, authorId, null);
    }

    public BookDto(@JsonProperty("id") final Long id, @JsonProperty("title") final String title,
                   @JsonProperty("authorId") final Long authorId, @JsonProperty("authorName") final String authorName) {
        this.id = id;
        this.title = title;
        this.authorId = authorId;
        this.authorName = authorName;
    }

    public BookDto() {
    }
}
