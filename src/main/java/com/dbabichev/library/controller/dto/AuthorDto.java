package com.dbabichev.library.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Copyright 2017 dbabichev
 * User: Dmytro_Babichev
 * Date: 8/30/2017
 * Time: 12:56 PM
 */
public class AuthorDto {
    public Long id;
    @NotBlank(message = "Provide non-blank author's name")
    public String name;

    public AuthorDto(final String name) {
        this(null, name);
    }

    public AuthorDto(@JsonProperty("id") final Long id, @JsonProperty("name") final String name) {
        this.id = id;
        this.name = name;
    }

    public AuthorDto() {
    }
}
