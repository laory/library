package com.dbabichev.library.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Copyright 2017 dbabichev
 * User: dbabichev
 * Date: 02/9/17
 * Time: 7:31 PM
 */
@Controller
public class CustomErrorController implements ErrorController {

    @GetMapping("/error")
    public String error(final HttpServletRequest request, final Map<String, Object> model) {
        final Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        final HttpStatus status = HttpStatus.valueOf(statusCode);
        model.put("status", statusCode);
        model.put("message", status.getReasonPhrase());
        return "errorPage";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
