var BOOKS_LIST_PAGE = "/booksList";
var AUTHORS_LIST_PAGE = "/authorsList";

function updateBook() {
    var bookId = $("input[name=bookId]").val();
    var title = $("input[name=title]").val();
    var authorId = $("select[name=authorId]").val();
    $.ajax({
        url: "/books/" + bookId,
        method: "PUT",
        data: JSON.stringify({
            title: title,
            authorId: authorId
        }),
        contentType: "application/json"
    }).done(() => {
        window.location.replace(BOOKS_LIST_PAGE);
    });
}

function deleteBook(bookId) {
    $.ajax({
        url: "/books/" + bookId,
        method: "DELETE"
    }).done(() => {
        window.location.replace(BOOKS_LIST_PAGE);
    });
}

function updateAuthor() {
    var authorId = $("input[name=authorId]").val();
    var name = $("input[name=name]").val();
    $.ajax({
        url: "/authors/" + authorId,
        method: "PUT",
        data: JSON.stringify({
            name: name
        }),
        contentType: "application/json"
    }).done(() => {
        window.location.replace(AUTHORS_LIST_PAGE);
    });
}

function deleteAuthor(authorId) {
    $.ajax({
        url: "/authors/" + authorId,
        method: "DELETE"
    }).done(() => {
        window.location.replace(AUTHORS_LIST_PAGE);
    });
}

$(document).ready(() => {
    $("#createBookForm").on("submit", function(e) {
        e.preventDefault();
        var json = httpFormToJson($(this));
        $.ajax({
            url: "/books/",
            method: "POST",
            data: JSON.stringify(json),
            contentType: "application/json"
        }).done(() => {
            window.location.replace(BOOKS_LIST_PAGE);
        });
    });
    $("#createAuthorForm").on("submit", function(e) {
        e.preventDefault();
        var json = httpFormToJson($(this));
        $.ajax({
            url: "/authors/",
            method: "POST",
            data: JSON.stringify(json),
            contentType: "application/json"
        }).done(() => {
            window.location.replace(AUTHORS_LIST_PAGE);
        });
    });
});

function httpFormToJson(form) {
    var formArray = form.serializeArray();
    var json = {};
    $.each(formArray, (i, v) => {
        json[v.name] = v.value;
    });
    return json;
}